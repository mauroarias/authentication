package com.etcsoft.auth.ui.provider;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class PropertySourcesPlaceholderConfigProvider {
    public final static String CUSTOM_PROPERTY_SOURCE_PLACEHOLDER = "custom-property-source-placeholder";

    private final static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfig =
            new PropertySourcesPlaceholderConfigurer();

    @Bean(name = CUSTOM_PROPERTY_SOURCE_PLACEHOLDER)
    @Primary
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return propertySourcesPlaceholderConfig;
    }
}
