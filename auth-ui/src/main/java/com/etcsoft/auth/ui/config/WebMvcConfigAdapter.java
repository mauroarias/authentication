package com.etcsoft.auth.ui.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import static com.etcsoft.auth.ui.provider.PropertySourcesPlaceholderConfigProvider.CUSTOM_PROPERTY_SOURCE_PLACEHOLDER;

@Configuration
@EnableWebMvc
public class WebMvcConfigAdapter extends WebMvcConfigurerAdapter {

    private final PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer;

    @Autowired
    WebMvcConfigAdapter(@Qualifier(CUSTOM_PROPERTY_SOURCE_PLACEHOLDER)
                        final PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer) {
        this.propertySourcesPlaceholderConfigurer = propertySourcesPlaceholderConfigurer;
    }

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addViewController("/").setViewName("forward:/index");
        registry.addViewController("/index");
        registry.addViewController("/securedPage");
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("/resources/");
    }

}