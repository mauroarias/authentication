package com.etcsoft.auth.common.model;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Value
@Builder
public class UserModel {
    private String username;
    private String password;
    private String profile;
    @Singular
    private List<GrantedAuthority> authorities;
}
