package com.etcsoft.auth.common.repository;


import com.etcsoft.auth.common.model.UserModel;

import java.util.Optional;

public interface UserDao {

    Optional<UserModel> getUser(String username);
}
