package com.etcsoft.auth.common.factory;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;

import javax.sql.DataSource;

import static com.etcsoft.auth.common.factory.HikariDataSourcesProvider.OAUTH2_DATASOURCE;

@Configuration
public class JdbcAuthorizationCodeServicesProvider {
    public final static String CUSTOM_JDBC_AUTH = "custom-jdbc-auth";

    private final DataSource dataSource;

    @Autowired
    JdbcAuthorizationCodeServicesProvider(@Qualifier(OAUTH2_DATASOURCE) final HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean(name = CUSTOM_JDBC_AUTH)
    @Primary
    protected AuthorizationCodeServices authorizationCodeServices() {
        return new JdbcAuthorizationCodeServices(dataSource);
    }
}
