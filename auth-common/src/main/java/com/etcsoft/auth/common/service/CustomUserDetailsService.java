package com.etcsoft.auth.common.service;

import com.etcsoft.auth.common.model.CustomUser;
import com.etcsoft.auth.common.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserDao userDAO;

    @Autowired
    CustomUserDetailsService(@Qualifier("mysql") final UserDao userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        return userDAO
                .getUser(username)
                .map(CustomUser::new)
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));
    }

}