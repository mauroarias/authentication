package com.etcsoft.auth.common.factory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderProvider {
    public final static String CUSTOM_PASSWORD_ENCODER = "custom-password-encoder";

    @Bean(name = CUSTOM_PASSWORD_ENCODER)
    @Primary
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
}
