package com.etcsoft.auth.common.factory;

import com.etcsoft.auth.common.config.SecurityJwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
public class JwtAccessTokenConverterProvider {
    public final static String CUSTOM_JWT_ACCESS_TOKEN = "custom-jwt-access-token";

    private final SecurityJwtConfig signatureKeyConfig;

    @Autowired
    JwtAccessTokenConverterProvider(final SecurityJwtConfig signatureKeyConfig) {
        this.signatureKeyConfig = signatureKeyConfig;
    }

    @Bean(name = CUSTOM_JWT_ACCESS_TOKEN)
    @Primary
    public JwtAccessTokenConverter tokenEnhancer() {
        final JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        if(signatureKeyConfig.isSigned()) {
            converter.setSigningKey(signatureKeyConfig.getSignedKey());
        }
        return converter;
    }
}
