package com.etcsoft.auth.common.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class SecurityMysqlOauth2EndPoint {
    private final String user;
    private final String passwd;
    private final String host;
    private final String databaseName;
    private final int port;

    public SecurityMysqlOauth2EndPoint(final @Value("${security.mysql.oauth2.username}") String mysqlUser,
                                       final @Value("${security.mysql.oauth2.password}") String mysqlPasswd,
                                       final @Value("${security.mysql.oauth2.database}") String databaseName,
                                       final @Value("${security.mysql.oauth2.host}") String mysqlHost,
                                       final @Value("${security.mysql.oauth2.port}") Integer mysqlPort) {
        this.host = mysqlHost;
        this.passwd = mysqlPasswd;
        this.port = mysqlPort == null ? 3306 : mysqlPort;
        this.user = mysqlUser;
        this.databaseName = databaseName;
    }
}