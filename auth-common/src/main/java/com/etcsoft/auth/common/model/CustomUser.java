package com.etcsoft.auth.common.model;

import org.springframework.security.core.userdetails.User;

public class CustomUser extends User {

    public CustomUser(UserModel userModel) {
        super(userModel.getUsername(), userModel.getPassword(), userModel.getAuthorities());
    }
}