package com.etcsoft.auth.common.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import static com.etcsoft.auth.common.factory.JwtAccessTokenConverterProvider.CUSTOM_JWT_ACCESS_TOKEN;

@Configuration
public class TokenStoreProvider {
    public final static String CUSTOM_TOKEN_STORE = "custom-token-store";

    private final JwtAccessTokenConverter tokenConverter;

    @Autowired
    TokenStoreProvider(@Qualifier(CUSTOM_JWT_ACCESS_TOKEN) final JwtAccessTokenConverter tokenConverter) {
        this.tokenConverter = tokenConverter;
    }

    @Bean(name = CUSTOM_TOKEN_STORE)
    @Primary
    public TokenStore tokenStore() {
        return new JwtTokenStore(tokenConverter);
    }
}
