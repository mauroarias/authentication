package com.etcsoft.auth.server.config;

import com.etcsoft.auth.common.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.etcsoft.auth.common.factory.PasswordEncoderProvider.CUSTOM_PASSWORD_ENCODER;

@Configuration
public class WebSecurityConfigAdapter extends WebSecurityConfigurerAdapter {
    public final static String CUSTOM_AUTH_MANAGER = "custom-auth-manager";

    private final CustomUserDetailsService customDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    WebSecurityConfigAdapter(final CustomUserDetailsService customDetailsService,
                             @Qualifier(CUSTOM_PASSWORD_ENCODER) final PasswordEncoder passwordEncoder) {
        this.customDetailsService = customDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Bean(name = CUSTOM_AUTH_MANAGER)
    @Primary
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/login", "/oauth/authorize")
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll();
//        http.authorizeRequests()
//                .antMatchers("/api/**").permitAll()
//                .and()
//                .csrf().disable()
//                .anonymous().disable()
//                .authorizeRequests();
    }
}