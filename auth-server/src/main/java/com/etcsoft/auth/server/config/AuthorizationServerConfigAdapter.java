package com.etcsoft.auth.server.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;
import java.util.Arrays;

import static com.etcsoft.auth.common.factory.HikariDataSourcesProvider.OAUTH2_DATASOURCE;
import static com.etcsoft.auth.common.factory.JwtAccessTokenConverterProvider.CUSTOM_JWT_ACCESS_TOKEN;
import static com.etcsoft.auth.common.factory.PasswordEncoderProvider.CUSTOM_PASSWORD_ENCODER;
import static com.etcsoft.auth.common.factory.TokenStoreProvider.CUSTOM_TOKEN_STORE;
import static com.etcsoft.auth.server.config.WebSecurityConfigAdapter.CUSTOM_AUTH_MANAGER;


@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfigAdapter extends AuthorizationServerConfigurerAdapter {

    private final AuthenticationManager authenticationManager;
    private final TokenStore tokenStore;
    private final JwtAccessTokenConverter jwtTokenConverter;
    private final DataSource oauth2DataSource;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    AuthorizationServerConfigAdapter(@Qualifier(CUSTOM_TOKEN_STORE) final TokenStore tokenStore,
                                     @Qualifier(CUSTOM_JWT_ACCESS_TOKEN) final JwtAccessTokenConverter jwtTokenConverter,
                                     @Qualifier(CUSTOM_AUTH_MANAGER) final AuthenticationManager authenticationManager,
                                     @Qualifier(CUSTOM_PASSWORD_ENCODER) final PasswordEncoder passwordEncoder,
                                     @Qualifier(OAUTH2_DATASOURCE) final HikariDataSource oauth2DataSource) {
        this.tokenStore = tokenStore;
        this.jwtTokenConverter = jwtTokenConverter;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.oauth2DataSource = oauth2DataSource;
    }

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        enhancerChain.setTokenEnhancers(Arrays.asList(jwtTokenConverter));

        endpoints
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore)
                .accessTokenConverter(jwtTokenConverter)
                .tokenEnhancer(enhancerChain);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

        clients.jdbc(oauth2DataSource).passwordEncoder(passwordEncoder);
    }

}